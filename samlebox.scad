use <..\my_library.scad>
resolution=20;
E=0.001;
fn=20;

box_x=54;
box_dx=2;
box_y=65;
box_z=15;
box_d=1.5;
edgeh=2;
lid_z=5;

pcb_x=40;
pcb_y=60;
pcb_z=1.6;
pcb_hd=2;
pcb_hx=17.1;
pcb_hy=27.2;
lift=5.5;

pin_space=2.54;


screw_r_base=1.1;
screw_r_lid=1.4;
        

function pinx(n) = (-7.5+n)*pin_space;   
function piny(n) = (-10.5+n)*pin_space;

module cyl(x) {
    cylinder(x[0],x[1],x[1],$fn=fn);
}
module pipe(x) {
    difference() {
        cylinder(x[0],x[1],x[1],$fn=fn);
        translate([0,0,-E])
            cylinder(x[0]+2*E,x[2],x[2],$fn=fn);
    }
}

module box(wx,wy,wz,dx,d) {
    module base() {
        translate([dx,0,d/2])
          cube([wx,wy,d],center=true);
    }

    module walls(h) {
        translate([wx/2+dx,0,h/2+d/2])
            cube([d,wy,h],center=true);
        translate([-wx/2+dx,0,h/2+d/2])
            cube([d,wy,h],center=true);
        translate([dx,wy/2,h/2+d/2])
            cube([wx,d,h],center=true);
        translate([dx,-wy/2,h/2+d/2])
            cube([wx,d,h],center=true);
    }

    module corners(h) {
        translate([wx/2+dx,0,d/2]) rotate([90,0,0])
            cylinder(wy,d/2,d/2,$fn=20,center=true);
        translate([-wx/2+dx,0,d/2]) rotate([90,0,0])
            cylinder(wy,d/2,d/2,$fn=20,center=true);
        translate([dx,wy/2,d/2]) rotate([0,90,0])
            cylinder(wx,d/2,d/2,$fn=20,center=true);
        translate([dx,-wy/2,d/2]) rotate([0,90,0])
            cylinder(wx,d/2,d/2,$fn=20,center=true);
        
        module corn1(dx,dy) {
            translate([dx,dy,d/2]) {
                translate([0,0,h/2])
                    cylinder(h,d/2,d/2,$fn=20,center=true);
                sphere(d=d,$fn=20);
            }
        }
        for (i=[-.5:1]) for (j=[-.5:1]) {
            corn1(i*wx+dx,j*wy);
        }
    }
    
    module screwholes(h,dhigh=5,dlow=1,screwr=1) {
        wedgew=7;
        wedgevert=2;
        
        dy=(wy-wedgew)/2-d/2;
        a1=[-box_x/2+box_dx+box_d/2+dlow/2,0,.4+box_d];
        a2=[-box_x/2+box_dx+box_d/2+dhigh/2,0,h+box_d/2-wedgevert/2+edgeh];
        
        b1=[box_x/2+box_dx-box_d/2-dlow/2,0,.4+box_d];
        b2=[box_x/2+box_dx-box_d/2-dhigh/2,0,h+box_d/2-wedgevert/2+edgeh];
        
        module wedge(pos_bottom, pos_top,) {
            //color("red")
            difference() {
                hull() {
                    translate(pos_bottom)
                        cube([dlow,wedgew,1],center=true);
                    translate(pos_top)
                        cube([dhigh,wedgew,wedgevert],center=true);
                }
                translate(pos_top)
                    cylinder(8,screwr,screwr, center=true, $fn=20);
            }
        }
        wedge(a1,a2);
        wedge(b1+[0,dy,0],b2+[0,dy,0]);
        wedge(b1-[0,dy,0],b2-[0,dy,0]);
    }
    
    module screws(screwr=1) {
        width=7;
        dhigh=5;
        dy=(wy-width)/2-d/2;
        dx=box_x/2-box_d/2-dhigh/2;
        module screw(x,y) {
            translate([x,y,0]) {
                cylinder(2*d+.1,screwr,screwr,$fn=20,center=true);
                cylinder(4,4,.5,$fn=20,center=true);
            }
        }
        screw(-dx+box_dx,0);
        screw( dx+box_dx,-dy);
        screw( dx+box_dx,dy);
    }
    
    module edge_inner(h,delta=0) {
        translate([dx,0,h+d/2])
            linear_extrude(edgeh+delta) {
                difference() {
                    offset(r=d/2,$fn=20)
                        square([wx-d,wy-d],center=true);
                    square([wx-d-delta,wy-d-delta],center=true);
                }
            }
    }
    
    module wing() {
        w=15;
        h=3;
        r=4;
        d=2;
        rscrew=1.5;
        
        module screw() {
            cylinder(2*h+.1,rscrew,rscrew,$fn=20,center=true);
            translate([0,0,.8])
            cylinder(5,0,5,$fn=20,center=true);
        }
        
        translate([wx/2,0,h/2]) {
            difference() {
                hull() {
                    cube([.1,w,h],center=true);
                    translate([r+d,0,0])
                        cylinder(h,r,r,$fn=fn,center=true);
                }
                translate([r+d,0,0])
                    color("green") screw();
            }
        }
    }
    
    module pressure_support() {
        extra=0.1;
        h=box_z+lid_z-box_d-pcb_z-lift-extra;
        r1=4;
        r2=3;
        
        module support(x,y) {
            //color("red")
                translate([pinx(x),piny(y),box_d])
                    scale([.7,1,1]) 
                        cylinder(h,r1,r2);
        }
        support(1,7);
        support(14,14);
        
        // DEBUG
        // dd=5.75-edgeh+9.4;
        // color("green")
        // translate([0,0,dd/2]) cube([80,15,dd],center=true);
    }
 
    base();
    walls(wz);
    corners(wz);
    screwholes(wz, screwr=screw_r_base);
    edge_inner(wz);
    
    translate([0,wy+5,0]) {
        difference() {
            union() {
                base();
                walls(lid_z);
                corners(lid_z);
                screwholes(1,dlow=5,screwr=screw_r_lid);
            }
            edge_inner(lid_z-edgeh,delta=.01);
            screws(screwr=screw_r_lid);
        }
        translate([dx,0,0]) wing();
        translate([dx,0,0]) rotate([0,0,180]) wing();
        pressure_support();
    }
}


  
module plate() {
    module hole(x,y) {
        translate([x*pcb_hx,y*pcb_hy,0])
            cylinder(2*pcb_z,pcb_hd/2,pcb_hd/2,$fn=20,center=true);

    }
    translate([0,0,pcb_z/2+box_d+lift]) color("#00aa00") {
        difference() {
            cube([pcb_x,pcb_y,pcb_z],center=true);
            for (i=[-1:2:1]) for (j=[-1:2:1]) {
              hole(i,j);
            }
        }
    }
    color("#ffffff")
      for (i=[1:14]) for (j=[1:20])
        translate([pinx(i),piny(j),box_d+lift-.05*pcb_z])
          pipe([1.1*pcb_z,1.0,.5]);
}


module spacers() {
    for (i=[-1:2:1]) for (j=[-1:2:1]) 
        translate([i*pcb_hx,j*pcb_hy,box_d])
            pipe([lift,2.5,.9]);
}

module stikflad(deltaxy=0, dz=8, deltaz=0) {
    dx=6.1;
    dy=55.6;
    by=4;
    bx=1;
    translate([pinx(3.5),0,dz/2+deltaz]) {
        cube([dx+deltaxy,dy+deltaxy,dz],center=true);
        translate([-(dx+bx-E)/2,0,0])
            cube([bx+deltaxy,by+deltaxy,dz],center=true);
    }
}
//stikflad();

module text_temperatur() {
    translate([0,-25,.1])
    rotate([180,0,0])
    color("#333333")
    linear_extrude(.4)
    text("Temperatur",size=2,$fn=30,spacing=1.2);
}
module text_gps() {
    translate([5,-10,.5])
    rotate([180,0,90])
//    color("#333333")
    linear_extrude(.6)
    text("GPS",size=3,$fn=30,spacing=1.2,
      font="Liberation Sans:style=Bold");
}
//text_temperatur();
//text_gps();

module stikpin(px,py,num,deltaxy=0,dz=7,deltaz=0,color="#cccccc") {
    aligndif=-.3;
    dx=6+deltaxy;
    dy=2.54*num+2.55+deltaxy;
//  translate([pinx(px)+aligndif,piny(py),(box_d/2+dz+deltaz)/2]) {
    translate([pinx(px)+aligndif,piny(py),dz/2+deltaz]) {
//        color(color) {
            cube([dx,dy,dz],center=true);
//        }
    }
}

module bigled(px,py) {
    translate([pinx(px),piny(py),0]) {
        color("#ff0000") {
            cylinder(9,2.6,2.6,$fn=20,center=true);
        }
    }
}
module smallled(px,py,deltar=0,dz=4,deltaz=0) {
    translate([pinx(px),piny(py),dz/2+box_d+deltaz]) {
        cylinder(dz,deltar+1.6,deltar+1.6,$fn=20,center=true);
    }
}


module ledspacer(n) {
    
    ledspace=1.5;
    ledbase=1.1;
    width=3.5;
    spaceh=lift-ledspace-ledbase;
    difference() {
        hull() {
            translate([(n-1)*pin_space+.5,0,spaceh/2])
                cylinder(spaceh,width/2,width/2,$fn=fn,center=true);
            translate([(1-n)*pin_space-.5,0,spaceh/2])
                cylinder(spaceh,width/2,width/2,$fn=fn,center=true);
        }
        for (dx=[-(n-1)/2:(n-1)/2]) {
            for (dy=[-pin_space/2-.3,pin_space/2+.3])
                translate([dx*2*pin_space,dy,spaceh/2-E])
                    //cylinder(spaceh+3*E,r,r,center=true,$fn=fn);
                    cube([1,2,spaceh+3*E],center=true);
        }
    }
}

module push_buttons(deltar=0,deltaz=-1,h=4) {
    x=14;
    py=[13,16];
    for (y=py)
    translate([pinx(x),piny(y),deltaz+h/2]) 
        cylinder(h,1.62+deltar,1.62+deltar,center=true,$fn=fn);
}

//translate([5*pin_space,9*pin_space,5])
translate([0,-40,0])
ledspacer(5);


ledy=19.5;
ledx=[8,10,12,14,16];

xgps=8;
ygps=8.5;

module build_all() {
    stikpin(8,3,3,deltaxy=1.7,dz=4);
    stikpin(11,3,3,deltaxy=1.7,dz=4);
    stikpin(14,3,3,deltaxy=1.7,dz=4);
    
    stikpin(xgps,ygps,4, deltaxy=1.7, dz=4);

    for (x=ledx) 
      smallled(x,ledy,deltar=1,dz=1.5);
    //smallled(7.1,21, deltar=1,dz=3.5);
    //smallled(8.7,21, deltar=1,dz=3.5);
    //smallled(ledx,ledy,deltar=1,dz=3.5);
    //smallled(12,8.5,deltar=1,dz=3.5);
    //smallled(12,10.5,deltar=1,dz=3.5);

    stikflad(deltaxy=2.2, dz=4);
    box(box_x,box_y,box_z,box_dx,box_d);
    
    spacers();
    push_buttons(deltar=1,deltaz=.1,h=2);
}

difference() {
    build_all();
    //text_temperatur();
    text_gps();
    stikflad(.3,deltaz=-.1);
    // Temperatur
    stikpin(8,3,3,deltaz=-.1);
    stikpin(11,3,3,deltaz=-.1);
    stikpin(14,3,3, deltaz=-.1);
    // GPS 
    stikpin(xgps,ygps,4, deltaz=-.1);
    for (x=ledx) 
      smallled(x,ledy,deltaz=-.1-box_d);
    push_buttons(deltar=.1);
    //smallled(8.7,21,deltaz=-.1);
    // Power
    //smallled(ledx,ledy,deltaz=-.1);
    //smallled(12,10.5,deltaz=-.1);
}

module components() {
  color("black") push_buttons();
  color("blue")   smallled(ledx[0],ledy,deltaz=-box_d-.5);
  color("green")  smallled(ledx[1],ledy,deltaz=-box_d-.5);
  color("yellow") smallled(ledx[2],ledy,deltaz=-box_d-.5);
  color("red")    smallled(ledx[3],ledy,deltaz=-box_d-.5);
  color("white")  stikpin(8,3,3,deltaz=-.1);
}

//plate();

//color("#555555")
//    stikflad(dz=6,deltaz=2);
