# piConnectorBox

I need a flexible and easy way to connect stuff to my Rapberry Pi caravan server. 
This box enables me to connect/disconnect each component as well as the Pi itself.

It also gives me some status LEDs and push buttons which can help with various stuff.

This first edition has 3 3-pin JST connecters for one-wire temperature sensors and
a 4 pin JST for my GPS. The Raspberry pi is connected by a standard 40-wire ribbon
cable.

Connecting this way ensures that I don't accidentally mix stuff up.
